#! /usr/bin/env python3

import os
import hvac
import gitlab
import urllib3
import argparse
import time


urllib3.disable_warnings()


def get_args():

    parser = argparse.ArgumentParser(description="Sync your self-hosted Gitlab Repos with Hosted Gitlab")

    parser.add_argument(
        '--self-hosted-url',
        action='store',
        dest='self_hosted_url',
        help='URL to your self-hosted gitlab instance (eg gitlab.homelab.com)'
    )

    parser.add_argument(
        '--token-secret-path',
        action='store',
        dest='secret_path',
        help='Path to the token paths in vault'
    )

    parser.add_argument(
        '--username',
        dest='username',
        help='gitlab.com username'
    )

    return parser.parse_args()


if __name__ == '__main__':

    args = get_args()

    print(args.__dict__)

    vault = hvac.Client()

    gitlab_secrets = vault.read(args.secret_path)['data']['data']

    self_hosted_token = gitlab_secrets['gitlab_homelab_key']
    hosted_token = gitlab_secrets['gitlab_hosted_key']

    print('Connecting to self hosted gitlab')
    self_hosted_gl = gitlab.Gitlab(f'https://{args.self_hosted_url}', private_token=self_hosted_token, ssl_verify=False)

    print('Connecting to hosted Gitlab')
    hosted_gl = gitlab.Gitlab('https://gitlab.com', private_token=hosted_token)


    user_projects = self_hosted_gl.projects.list(owned=True)

    host_gl_projects = hosted_gl.projects.list(owned=True)

    for pj in user_projects:
        
        mirrors = pj.remote_mirrors.list()

        if mirrors == []:
            print(f'{pj.name} has no mirrors, setting one up in Gitlab')
            
            print(f'Checking if {pj.name} is already created in Gitlab')

            hosted_pj = [project for project in host_gl_projects if project.name == pj.name]

            if hosted_pj == []:
                print('Creating new repo')
                hosted_pj = hosted_gl.projects.create({
                    'name': pj.name,
                    'description': pj.description,
                    'visibility': pj.visibility
                })
                print('Waitng for new repo to be created')
                time.sleep(10)
            
            url = hosted_pj[0].attributes['http_url_to_repo'].replace('https://', '')
            print(f'Setting up sync to {url}')

            try:
                pj.remote_mirrors.create({
                    'url': f'https://{args.username}:{hosted_token}@{url}',
                    'enabled': True
                })
            except Exception as e:
                print(f'Error creating mirror for {pj.name}: {e}')
        else:
            print(f'{pj.name} already as a mirror setup')

    print('Script completed')

        