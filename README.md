# gitlab_sync

Sync repositories from self-hosted Gitlab to hosted Gitlab


## Running the script

Clone it from the repo

```bash
git clone git@gitlab.com:breid4241/gitlab_sync.git
cd gitlab_sync
```

Install the required packages
```bash
pip3 install -r requirements.txt
```

Create your environment variables
```bash
read -s self_hosted_token; export self_hosted_token
read -s hosted_token; export hosted_token
```

Then run the script

```bash
./gitlab_sync.py --self-hosted-token self_hosted_token --hosted-token hosted_token --self-hosted-url <self-hosted gitlab url without https://>

# Example
./gitlab_sync.py --self-hosted-token sl --hosted-token hl --self-hosted-url gitlab.homelab.com
```

## Docker

You can also run the script via a docker container

```bash
docker pull registry.gitlab.com/breid4241/gitlab_sync/main

docker run -e SELF_HOSTED_TOKEN=$sl -e HOSTED_TOKEN=$hl -e SELF_HOSTED_URL=gitlab.homelab.com -e USERNAME=breid4241 gitlab-sync
```