FROM python:3
WORKDIR /opt
COPY ./gitlab_sync.py gitlab_sync.py
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt --user

CMD python gitlab_sync.py --token-secret-path $TOKEN_PATH --self-hosted-url $SELF_HOSTED_URL --username $USERNAME
